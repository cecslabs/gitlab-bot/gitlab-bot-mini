# frozen_string_literal: true

require 'labkit/logging'
require 'labkit/context'
require 'json'

module Gitlab
  class JsonLogger < ::Labkit::Logging::JsonLogger
    class << self
      def file_name_noext
        raise NotImplementedError, 'JsonLogger implementations must provide file_name_noext implementation'
      end

      def file_name
        "#{file_name_noext}.log"
      end

      def debug(message)
        build.debug(message)
      end

      def error(message)
        build.error(message)
      end

      def warn(message)
        build.warn(message)
      end

      def info(message)
        build.info(message)
      end

      def build
        @log_map ||= {}
        @log_map[cache_key] ||= new(full_log_path, level: log_level)
      end

      def cache_key
        "logger:#{full_log_path}"
      end

      def full_log_path
        project_root = File.expand_path('../..', __dir__)
        log_path = File.join(project_root, 'log')
        full_log_path = File.join(log_path, file_name)
      end
    end

    private

    # Override Labkit's default impl, which uses the default Ruby platform json module.
    def dump_json(data)
      JSON.dump(data)
    end
  end
end
