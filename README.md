# Lucy's Robot
## What is this project?
This project functions as a bot that responds to comments made by GitLab users through webhooks, fulfilling requests with specific commands.

## Why it is called Lucy's Robot
Because [Suchman, Lucy](https://en.wikipedia.org/wiki/Lucy_Suchman) suggests to call it Lucy's Robot.

## Demo
![Demo Video](doc/vid/demo.mp4)

## Local development
Please refer to [local dev documentation](doc/dev.md).

## Preproduction
Please refer to [staging documentation](doc/staging.md).

## Release
Please refer to [prod documentation](doc/prod.md).

## I am interested in the project, and how can I contribute?
Please refer to [contribution documentation](doc/contribution.md).
