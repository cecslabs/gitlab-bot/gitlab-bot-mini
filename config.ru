# frozen_string_literal: true

require_relative 'general/rack/app'

run General::Rack::App.app
