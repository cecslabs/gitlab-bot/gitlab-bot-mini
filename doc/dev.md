## How to run the test server on Gitpod (recommand)
use gitpod to open this repository
```bash
export GITLAB_API_ENDPOINT='https://gitlab.cecs.anu.edu.au/api/v4'
export GITLAB_WEBHOOK_TOKEN='strong_token'
## This value would be the PAT of the bot user
export GITLAB_API_TOKEN=''
## This value would be the PAT of an admin user. Some of the command will need admin privilege to operate.
export GITLAB_API_ADMIN_TOKEN=''
export GITLAB_BOT_USERID='10347'
export DRY_RUN='no'
puma
```

There will be a url published after running puma server. Make that server to be public and create a webhook at gitlab. Make sure that the value of the secret token can match `GITLAB_WEBHOOK_TOKEN`. Also, tick that `Comments` trigger.

### How to run tests on local dev
```bash
## run a single spec file
rspec spec/general/processor/*_spec.rb
```

```bash
robocup
```

### How to fix rubocop error locally

```bash
## safe fix
rubocop -a
```

```bash
## unsafe fix, use with care.
rubocop -A
```

## How to run the server on the host machine
```bash
bundle install
export GITLAB_API_ENDPOINT='https://gitlab.cecs.anu.edu.au/api/v4'
export GITLAB_WEBHOOK_TOKEN=''
export GITLAB_API_TOKEN=''
export GITLAB_API_ADMIN_TOKEN=''
export GITLAB_BOT_USERID='10347'
export DRY_RUN='no'
puma
```
The webserver is running on port 2000, and it needs to be exposed to gitlab server. Create a webhook at gitlab. Make sure that the value of the secret token can match `GITLAB_WEBHOOK_TOKEN`. Also, tick that `Comments` trigger.

## How to test it with docker
```bash
docker run -e GITLAB_API_ENDPOINT='https://gitlab.cecs.anu.edu.au/api/v4' -e GITLAB_WEBHOOK_TOKEN='' -e GITLAB_BOT_USERID='10347' -e GITLAB_API_TOKEN='' -e GITLAB_API_ADMIN_TOKEN='' -e DRY_RUN='no' -d -p 2000:2000 jiegao1025/bot:<version>
```
The webserver is running on port 2000, and it needs to be exposed to gitlab server. Create a webhook at gitlab. Make sure that the value of the secret token can match `GITLAB_WEBHOOK_TOKEN`. Also, tick that `Comments` trigger.


## How to add a new command
1. create a new ruby file at `general/processor/<command name>.rb`
2. at `general/general/handler.rb`, add `require_relative '../processor/<command name>'` in the top.
3. at `general/general/handler.rb`, add new `DEFAULT_PROCESSORS`.
