## How can I contribute to this project?
At the current stage while the project isn't fully stable, I just hope I can get more ideas about new bot commands.

## How can I communicate if I have a good idea of a new bot command?
Go to `Issues` -> `New issue`. Create a new issue and put it under Milestone: `create more bot commands`. Once done, please also @u6076069 for better visibilty.
