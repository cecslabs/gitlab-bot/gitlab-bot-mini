## How to build a production docker image after testing the new command on staging

#### CI/CD build (Recommanded way)
Manually trigger the deploy button after making sure there are no obvious security issues. The docker image would be called `vx.x`

### Manual build
```bash
docker login
docker build -t jiegao1025/bot:vx.x .
docker push jiegao1025/bot:vx.x
```

## How to announce a new release

Go to `https://gitlab.cecs.anu.edu.au/cecslabs/gitlab-bot/gitlab-bot-mini/-/releases` and click `New Release`.

Fulfill release notes and click `Create release`.
