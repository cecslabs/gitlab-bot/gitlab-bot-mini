## How to build a staging docker image after adding a new command
### CI/CD build (Recommanded way)
Once it is finish developing, tag it called `vx.x`. The pipeline would be triggered and build a docker image with `vx.x-staging`. The container security scanning would also be done. If everything seems okay, click the manual job to further deploy it to `vx.x`.
#### Container scanning
The pipeline will scan the docker image for the dependency list, common CVEs, credential leaks. It is suggested to address all critical CVE and potential credential leaks before pushing.

### Manual build
```bash
docker login
docker build -t jiegao1025/bot:vx.x-staging .
docker push jiegao1025/bot:vx.x-staging
```