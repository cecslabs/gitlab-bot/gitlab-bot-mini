# frozen_string_literal: true

require 'rack'
require 'openssl'
require 'digest/sha2'

require_relative '../logger/rack_logger'

module General
  module Rack

    Authenticator = Struct.new(:app) do
      attr_accessor :logger

      def initialize(app)
        @app = app
        @logger = General::RackLogger.build
      end


      def self.secure_compare(a, b)
        ::Rack::Utils.secure_compare(
          ::OpenSSL::Digest::SHA256.hexdigest(a),
          ::OpenSSL::Digest::SHA256.hexdigest(b)
        ) &&
          a == b
      end

      def call(env)
        if webhook_token_empty?
          return ::Rack::Response.new([JSON.dump(status: :unauthenticated, message: "Token wasn't provided")],
                                      400).finish
        end

        if authenticated?(env)
          @app.call(env)
        else
          @logger.error(info: 'Unauthenticated request!', payload: env)
          warn 'Unauthenticated request!'

          ::Rack::Response.new([JSON.dump(status: :unauthenticated)], 401).finish
        end
      end

      private

      def authenticated?(env)
        self.class.secure_compare(http_gitlab_token(env), webhook_token)
      end

      def http_gitlab_token(env)
        env['HTTP_X_GITLAB_TOKEN'].to_s
      end

      def webhook_token
        ENV['GITLAB_WEBHOOK_TOKEN'].to_s
      end

      def webhook_token_empty?
        webhook_token.empty?
      end
    end
  end
end
