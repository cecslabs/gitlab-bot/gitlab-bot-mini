# frozen_string_literal: true

require 'rack'
require 'rack/requestid'
require 'json'

require_relative 'authenticator'
require_relative 'webhook_event'
require_relative 'processor'

module General
  module Rack
    module App
      def self.app
        ::Rack::Builder.app do
          use ::Rack::RequestID # makes sure `env` has an X-Request-Id header

          use ::Rack::ContentType, 'application/json'
          use General::Rack::Authenticator
          use General::Rack::WebhookEvent
          run General::Rack::Processor.new
        end
      end
    end
  end
end
