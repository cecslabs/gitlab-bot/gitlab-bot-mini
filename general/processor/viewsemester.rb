# frozen_string_literal: true

require_relative '../general/processor'

module General
  class ViewSemester < Processor
    def process
      return unless validate_command_length(@event, 1, 'Wrong parameter. Please type help.')

      logger.info(command: 'ViewSemester', username: @event.event_actor_username)

      year = General::Util.current_year(Time.now.year, Date.today.month)
      semester = General::Util.current_semester(Date.today.month)

      return_comment = "The active semester is #{year}-#{semester}."
      add_comment(return_comment, append_source_link: false)
    end
  end
end
