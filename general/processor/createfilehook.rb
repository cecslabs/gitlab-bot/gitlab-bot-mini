# frozen_string_literal: true

require_relative '../general/processor'

module General
  class CreateFileHook < Processor
    def process
      require 'git'
      require 'yaml'

      if event.command.command_length == 3
        type = 'user'

        logger.info(command: 'CreateFileHook', command_param1: @event.command.command_param(1),
        command_param2: @event.command.command_param(2), username: @event.event_actor_username)

        course = @event.command.command_param(1)
        repo = @event.command.command_param(2)
      else
        return unless validate_command_length(@event, 4, 'Wrong parameter. Please type help.')

        logger.info(command: 'CreateFileHook', command_param1: @event.command.command_param(1),
        command_param2: @event.command.command_param(2), command_param3: @event.command.command_param(3),
        username: @event.event_actor_username)

        type = @event.command.command_param(1)
        course = @event.command.command_param(2)
        repo = @event.command.command_param(3)
      end

      return unless validate_filehook_type(type, 'Filehook type must be either "user", or "issue".')

      return unless validate_course(course, 'course must be (comp|engn|extn)[0-9]{4}.')

      if @event.project_namespace != course and @event.project_namespace != "comptest"
        add_comment(
          'Invalid command.', append_source_link: false
        )
        logger.error(command: 'CreateFileHook', message: "The namespace didn't match course or it is not under comptest group.",
                  username: @event.event_actor_username)
        return
      end

      path = repo.match(%r{^https://gitlab\.cecs\.anu\.edu\.au/(?!.*\.git$)(.*)$})

      if path.nil?
        add_comment(
          'repo must be https://gitlab.cecs.anu.edu.au/<path>', append_source_link: false
        )
        return
      end

      path = path[1]

      year = General::Util.current_year(Time.now.year, Date.today.month)
      semester = General::Util.current_semester(Date.today.month)

      marker = "#{course}-#{year}-#{semester}-marker"

      admin_token = ENV['GITLAB_API_ADMIN_TOKEN']

      result = `python3 ./general/command/gitlab-teaching-admin/viewuser.py -i https://gitlab.cecs.anu.edu.au -k #{admin_token} -u #{course}-#{year}-#{semester}-marker`
      result_hash = JSON.parse(result).first

      if result_hash.nil?
        add_comment('No such user. Please use `createmarker` to create a user.', append_source_link: false)
        return
      end

      ## Start to write filehook to the repositories depending on the type of filehook.
      case type
      when "user"
        create_user_filehook(admin_token, path, marker, course)
      when "issue"
        create_issue_filehook(admin_token, path, marker, course)
      end
    end

    private

    def create_user_filehook(admin_token, path, marker, course)
      FileUtils.rm_rf('gitlab-plugin')

      # Clone the repository to a local directory
      repo = Git.clone("https://root:#{admin_token}@gitlab.cecs.anu.edu.au/cecsit/gitlab-plugin.git", 'gitlab-plugin')

      # Read the YAML file from the cloned repository
      yaml_file = repo.show('master', 'adduser.yaml')
      yaml_data = YAML.safe_load(yaml_file)
      yaml_data[path] = [
        {
          'user' => marker,
          'role' => 'master'
        }
      ]
      File.write('gitlab-plugin/adduser.yaml', YAML.dump(yaml_data, sort_keys: true, line_width: -1))

      repo.add
      repo.config('user.name', "#{marker}")
      repo.config('user.email', 'gitlab-bot@anu.edu.au')
      repo.commit("for #{course}")
      repo.push

      add_comment('New filehook - create user - is created! Please wait for 30 minutes to have it deployed.', append_source_link: false)
    end

    def create_issue_filehook(admin_token, path, marker, course)
      FileUtils.rm_rf('gitlab-plugin')

      # Clone the repository to a local directory
      repo = Git.clone("https://root:#{admin_token}@gitlab.cecs.anu.edu.au/cecsit/gitlab-plugin.git", 'gitlab-plugin')

      # Read the YAML file from the cloned repository
      yaml_file = repo.show('master', 'copyissues.yaml')
      yaml_data = YAML.safe_load(yaml_file)
      yaml_data[path] = [
        {
          'user' => marker,
        }
      ]
      File.write('gitlab-plugin/copyissues.yaml', YAML.dump(yaml_data, sort_keys: true, line_width: -1))

      repo.add
      repo.config('user.name', "#{marker}")
      repo.config('user.email', 'gitlab-bot@anu.edu.au')
      repo.commit("for #{course}")
      repo.push

      add_comment('New filehook - copy issue - is created! Please wait for 30 minutes to have it deployed.', append_source_link: false)
    end
  end
end
