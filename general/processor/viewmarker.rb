# frozen_string_literal: true

require_relative '../general/processor'

module General
  class ViewMarker < Processor
    def process
      return unless validate_command_length(@event, 2, 'Wrong parameter. Please type help.')

      logger.info(command: 'ViewMarker', command_param: @event.command.command_param(1),
                  username: @event.event_actor_username)

      course = @event.command.command_param(1)

      return unless validate_course(course, 'course must be (comp|engn|extn)[0-9]{4}.')

      year = General::Util.current_year(Time.now.year, Date.today.month)
      semester = General::Util.current_semester(Date.today.month)

      username = @event.event_actor_username
      admin_token = ENV['GITLAB_API_ADMIN_TOKEN']

      result = `python3 ./general/command/gitlab-teaching-admin/viewuser.py -i https://gitlab.cecs.anu.edu.au -k #{admin_token} -u #{course}-#{year}-#{semester}-marker`
      result_hash = JSON.parse(result).first

      if result_hash.nil?
        add_comment('No such user. Please use `createmarker` to create a user.', append_source_link: false)
      else
        result_pretty_hash = JSON.pretty_generate(result_hash)
        return_comment = <<~EOF
          ```yaml
          #{result_pretty_hash}
          ```
        EOF
        add_comment(return_comment, append_source_link: false)
      end
    end
  end
end
