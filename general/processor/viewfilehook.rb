# frozen_string_literal: true

require_relative '../general/processor'

module General
  class ViewFileHook < Processor
    def process
      require 'git'
      require 'yaml'

      return unless validate_command_length(@event, 2, 'Wrong parameter. Please type help.')

      logger.info(command: 'ViewFileHook', command_param: @event.command.command_param(1),
                  username: @event.event_actor_username)

      course = @event.command.command_param(1)

      return unless validate_course(course, 'course must be (comp|engn|extn)[0-9]{4}.')

      admin_token = ENV['GITLAB_API_ADMIN_TOKEN']
      FileUtils.rm_rf('gitlab-plugin')

      # Clone the repository to a local directory
      repo = Git.clone("https://root:#{admin_token}@gitlab.cecs.anu.edu.au/cecsit/gitlab-plugin.git", 'gitlab-plugin')

      # Read the YAML file from the cloned repository
      yaml_file = repo.show('master', 'adduser.yaml')
      yaml_data = YAML.safe_load(yaml_file)
      ret_data = {}

      # Iterate over the key-value pairs in the data
      yaml_data.each do |key, value|
        # Check if the key includes the string "comp1100"
        ret_data["https://gitlab.cecs.anu.edu.au/#{key}"] = value if key.include?(course)
      end
      return_comment = <<~EOF
        ```yaml
        #{YAML.dump(ret_data)}
        ```
      EOF

      add_comment(return_comment.to_s, append_source_link: false)
    end
  end
end
