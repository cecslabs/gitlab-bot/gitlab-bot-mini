# frozen_string_literal: true

require_relative '../general/processor'

module General
  class DeleteFileHook < Processor
    def process
      require 'git'
      require 'yaml'

      return unless validate_command_length(@event, 3, 'Wrong parameter. Please type help.')

      logger.info(command: 'DeleteFileHook', command_param1: @event.command.command_param(1),
                  command_param2: @event.command.command_param(2), username: @event.event_actor_username)

      course = @event.command.command_param(1)

      return unless validate_course(course, 'course must be (comp|engn|extn)[0-9]{4}.')
      if @event.project_namespace != course and @event.project_namespace != "comptest"
        add_comment(
          'Invalid command.', append_source_link: false
        )
        logger.error(command: 'DeleteFileHook', message: "The namespace didn't match course or it is not under comptest group.",
                  username: @event.event_actor_username)
        return
      end

      repo = @event.command.command_param(2)

      path = repo.match(%r{^https://gitlab\.cecs\.anu\.edu\.au/(?!.*\.git$)(.*)$})

      if path.nil?
        add_comment(
          'repo must be https://gitlab.cecs.anu.edu.au/<path>', append_source_link: false
        )
        return
      end

      path = path[1]

      year = General::Util.current_year(Time.now.year, Date.today.month)
      semester = General::Util.current_semester(Date.today.month)

      marker = "#{course}-#{year}-#{semester}-marker"

      admin_token = ENV['GITLAB_API_ADMIN_TOKEN']

      result = `python3 ./general/command/gitlab-teaching-admin/viewuser.py -i https://gitlab.cecs.anu.edu.au -k #{admin_token} -u #{course}-#{year}-#{semester}-marker`
      result_hash = JSON.parse(result).first

      if result_hash.nil?
        add_comment('No such user. Please use `createmarker` to create a user.', append_source_link: false)
        return
      end

      FileUtils.rm_rf('gitlab-plugin')

      # Clone the repository to a local directory
      repo = Git.clone("https://root:#{admin_token}@gitlab.cecs.anu.edu.au/cecsit/gitlab-plugin.git", 'gitlab-plugin')

      # Read the YAML file from the cloned repository
      yaml_file = repo.show('master', 'adduser.yaml')
      yaml_data = YAML.safe_load(yaml_file)

      if yaml_data.key?(path)
        # Delete the key and its value
        yaml_data.delete(path)
      end

      File.write('gitlab-plugin/adduser.yaml', YAML.dump(yaml_data, sort_keys: true, line_width: -1))

      repo.add
      repo.config('user.name', "#{course}-#{year}-#{semester}-marker")
      repo.config('user.email', 'gitlab-bot@anu.edu.au')
      repo.commit("for #{course}")
      repo.push

      add_comment('Filehook is deleted!', append_source_link: false)
    end
  end
end
