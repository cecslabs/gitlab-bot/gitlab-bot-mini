# frozen_string_literal: true

require 'json'

require_relative '../general/processor'

module General
  class EditMarker < Processor
    def process
      return unless validate_command_length(@event, 4, 'Wrong parameter. Please type help.')

      logger.info(command: 'EditMarker', command_param1: @event.command.command_param(1),
                  command_param2: @event.command.command_param(2),
                  username: @event.event_actor_username)

      course = @event.command.command_param(1)

      if @event.project_namespace != course and @event.project_namespace != "comptest"
        add_comment(
          'Invalid command.', append_source_link: false
        )
        logger.error(command: 'EditMarker', message: "The namespace didn't match course or it is not under comptest group.",
                  username: @event.event_actor_username)
        return
      end

      return unless validate_course(course, 'course must be (comp|engn|extn)[0-9]{4}.')

      user_key = @event.command.command_param(2)
      user_value = @event.command.command_param(3)

      if user_key != 'projects_limit'
        add_comment(
          'We can only set up `projects_limit` now for the marker user.', append_source_link: false
        )
        return
      end

      if user_value.to_i <= 0 || user_value.to_i >= 10_000
        add_comment(
          'The project limit must be greater than 0 and smaller than 10000.', append_source_link: false
        )
        return
      end

      year = General::Util.current_year(Time.now.year, Date.today.month)
      semester = General::Util.current_semester(Date.today.month)

      username = @event.event_actor_username
      admin_token = ENV['GITLAB_API_ADMIN_TOKEN']

      result = `python3 ./general/command/gitlab-teaching-admin/edituser.py -i https://gitlab.cecs.anu.edu.au -k #{admin_token} -u #{course}-#{year}-#{semester}-marker --#{user_key} #{user_value}`

      result_hash = JSON.parse(result)

      if result.match(/error/)
        add_comment("There is an error when creating #{course}-#{year}-#{semester}-marker. The message is: #{result_hash['error']}",
                    append_source_link: false)
        return
      end

      add_comment(
        "The value of #{user_key} is changed to #{user_value} for #{course}-#{year}-#{semester}-marker. Use viewmarker to check the change.", append_source_link: false
      )
    end
  end
end
