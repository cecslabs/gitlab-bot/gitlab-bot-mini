# frozen_string_literal: true

require_relative '../general/processor'

module General
  class HelloBot < Processor
    def process
      logger.info(command: 'HelloBot', username: @event.event_actor_username)

      hello = <<~MARKDOWN.chomp
        Hi, I am an ANU GitLab Bot. I can only respond to command you typed.
        I am not ChatGPT so I can't understand complicated requests.
        Please don't play me as I am fragile.
        If you found I didn't respond your command, please email to jie.gao@anu.edu.au.
      MARKDOWN
      add_comment(hello, append_source_link: false)
    end
  end
end
