# frozen_string_literal: true

require 'json'

require_relative '../general/processor'

module General
  class CreateMarker < Processor
    def process
      return unless validate_command_length(@event, 2, 'Wrong parameter. Please type help.')

      logger.info(command: 'CreateMarker', command_param: @event.command.command_param(1),
                  username: @event.event_actor_username)

      course = @event.command.command_param(1)

      return unless validate_course(course, 'course must be (comp|engn|extn)[0-9]{4}.')
      if @event.project_namespace != course and @event.project_namespace != "comptest"
        add_comment(
          'Invalid command.', append_source_link: false
        )
        logger.error(command: 'CreateMarker', message: "The namespace didn't match course or it is not under comptest group.",
                  username: @event.event_actor_username)
        return
      end

      year = General::Util.current_year(Time.now.year, Date.today.month)
      semester = General::Util.current_semester(Date.today.month)

      username = @event.event_actor_username
      admin_token = ENV['GITLAB_API_ADMIN_TOKEN']

      result = `python3 ./general/command/gitlab-teaching-admin/adduser.py -i https://gitlab.cecs.anu.edu.au -k #{admin_token} -u #{course}-#{year}-#{semester}-marker --id #{username} --projectlimit 9000`

      result_hash = JSON.parse(result)

      if result.match(/message/)
        add_comment("There is an error when creating #{course}-#{year}-#{semester}-marker. The message is: #{result_hash['message']}",
                    append_source_link: false)
      else
        add_comment(
          "#{course}-#{year}-#{semester}-marker is created! There should be an email sent to your inbox for resetting password. If you cannot find it, please check the quarantine.", append_source_link: false
        )
      end
    end
  end
end
