# frozen_string_literal: true

require_relative '../general/job'
require_relative '../general/handler'
require_relative '../general/event'
require_relative '../general/util'

module General
  class ProcessorJob < Job
    private

    def execute(payload)
      prepare_executing_with(General::Event.build(payload))

      handler = Handler.new(event)

      return if event.event_actor_id.to_s == ENV['GITLAB_BOT_USERID'].to_s

      result = handler.process

      ::Rack::Response.new([JSON.dump(status: :ok, messages: result)]).finish
    end
  end
end
