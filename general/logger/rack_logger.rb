# frozen_string_literal: true

require_relative '../../lib/gitlab/json_logger'

module General
  class RackLogger < ::Gitlab::JsonLogger
    def self.file_name_noext
      'rack'
    end
  end
end
