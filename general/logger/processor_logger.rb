# frozen_string_literal: true

require_relative '../../lib/gitlab/json_logger'

module General
  class ProcessorLogger < ::Gitlab::JsonLogger
    def self.file_name_noext
      'processor'
    end
  end
end
