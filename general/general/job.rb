require 'sucker_punch'

module General
    class Job
      include SuckerPunch::Job
      workers 4

      attr_reader :event

      def self.drain!
        pool = SuckerPunch::Queue::QUEUES.delete(to_s)
  
        return true unless pool
  
        # See https://github.com/brandonhilkert/sucker_punch/blob/v3.0.1/lib/sucker_punch/queue.rb#L74
        deadline = Time.now + SuckerPunch.shutdown_timeout
  
        pool.shutdown
  
        remaining = deadline - Time.now
  
        while remaining > SuckerPunch::Queue::PAUSE_TIME
          break if pool.shutdown?
  
          sleep SuckerPunch::Queue::PAUSE_TIME
          remaining = deadline - Time.now
        end
  
        remaining.positive?
      end
  
      def perform(...)  
        execute(...)
      end
  
      private
  
      def prepare_executing_with(event)
        @event = event
        #logger.info('Executing job', job: self.class, resource_url: event.url)
      end
  
    end
end