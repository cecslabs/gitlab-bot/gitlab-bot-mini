# frozen_string_literal: true

require 'mini_cache'
require 'gitlab'

require_relative 'reaction'

module General
  class Util
    def self.current_monotonic_time
      Process.clock_gettime(Process::CLOCK_MONOTONIC, :float_second)
    end

    def self.dry_run?
      (ENV['DRY_RUN'] || '0').match?(/yes|1|true/i)
    end

    def self.cache
      @cache ||= MiniCache::Store.new
    end

    def self.api_client
      cache.get_or_set(:api_client) do
        Gitlab.client(endpoint: ENV['GITLAB_API_ENDPOINT'], private_token: ENV['GITLAB_API_TOKEN'].to_s)
      end
    end

    def self.current_year(current_year, current_month)
      year = current_year
      year += 1 if current_month == 12
      year = year.to_s
    end

    def self.current_semester(current_month)
      if [12, 1, 2, 3, 4, 5, 6].include?(current_month)
        semester = 's1'
      elsif [7, 8, 9, 10, 11].include?(current_month)
        semester = 's2'
      end
    end
  end
end
