# frozen_string_literal: true

require 'gitlab'

require_relative 'reaction'

require_relative '../logger/processor_logger'

module General
  class Processor
    include Reaction

    attr_reader :event
    attr_accessor :logger

    def initialize(event)
      @event = event
      @logger = General::ProcessorLogger.build
    end

    def self.react_to_approvals
      # Merge request event can contain either `approved` or `approval` action
      # depending on the approval rules set for the merge request.
      # In this case, the reaction applies to either action.
      react_to 'merge_request.approval', 'merge_request.approved'
    end

    def self.react_to(*events_def)
      events_def.each do |event_def|
        listener = Listener.listeners_for_event(*event_def.split('.'))
        listeners.concat(listener)
      end
    end

    def self.listeners
      @listeners ||= []
    end

    def self.triage(event)
      new(event).triage
    end

    def triage
      return unless applicable?

      before_process
      process.tap { after_process }
    end

    def process
      raise NotImplementedError
    end

    def documentation
      '' # Empty by default
    end

    private

    def applicable?
      true
    end

    def before_process
      nil
    end

    def after_process
      nil
    end

    # Override this method if a different class name is required
    def unique_comment
      @unique_comment ||= UniqueComment.new(self.class.name, event)
    end
  end
end
