# frozen_string_literal: true

require_relative 'util'

module General
  module Reaction
    PRODUCTION_API_ENDPOINT = ENV['GITLAB_API_ENDPOINT'] || 'https://gitlab.com/api/v4'

    module_function

    def validate_command_length(event, length, text)
      return true if event.command.command_length == length

      add_comment(
        text.to_s,
        append_source_link: false
      )
      false
    end

    def validate_course(course, text)
      return true if course.to_s.match(/^(comp|engn|extn)[0-9]{4}$/)

      add_comment(
        text.to_s, append_source_link: false
      )
      false
    end

    def validate_filehook_type(type, text)
      return true if type.to_s.match(/^(user|issue)$/)

      add_comment(
        text.to_s, append_source_link: false
      )
      false
    end

    def add_comment(body, noteable_path = event.noteable_path, append_source_link:)
      path = "#{noteable_path}/notes"

      Reaction.post_request(path, body, append_source_link: append_source_link)
    end

    def add_discussion(body, noteable_path = event.noteable_path, append_source_link:)
      path = "#{noteable_path}/discussions"

      Reaction.post_request(path, body, append_source_link: append_source_link)
    end

    def append_discussion(body, discussion_id, noteable_path = event.noteable_path, append_source_link:)
      path = "#{noteable_path}/discussions/#{discussion_id}/notes"

      Reaction.post_request(path, body, append_source_link: append_source_link)
    end

    def update_discussion_comment(body, discussion_id, note_id, noteable_path = event.noteable_path)
      path = "#{noteable_path}/discussions/#{discussion_id}/notes/#{note_id}"

      Reaction.put_request(path, body: body)
    end

    def resolve_discussion(discussion_id, noteable_path = event.noteable_path)
      path = "#{noteable_path}/discussions/#{discussion_id}"

      Reaction.put_request(path, resolved: true)
    end

    def unresolve_discussion(discussion_id, noteable_path = event.noteable_path)
      path = "#{noteable_path}/discussions/#{discussion_id}"

      Reaction.put_request(path, resolved: false)
    end

    def self.post_request(path, body, append_source_link: false)
      body = add_automation_suffix(body) if append_source_link

      Util.api_client.post(path, body: { body: body }) unless Util.dry_run?

      "POST #{PRODUCTION_API_ENDPOINT}#{path}, body: `#{body}`"
    end

    def self.put_request(path, body)
      Util.api_client.put(path, body: body) unless Util.dry_run?

      "PUT #{PRODUCTION_API_ENDPOINT}#{path}, body: `#{body}`"
    end

    def self.add_automation_suffix(body)
      source_path = get_source_path(caller_locations(1..5))
      return body unless source_path

      <<~MARKDOWN.chomp
        #{body}

        *This message was [generated automatically](https://about.gitlab.com/handbook/engineering/quality/triage-operations).
        You're welcome to [improve it](https://gitlab.com/gitlab-org/quality/triage-ops/-/blob/master/triage/processor/#{source_path}).*
      MARKDOWN
    end

    def get_source_path(locations)
      processor_location = locations.find { |location| location.path.include?('/triage/processor/') }
      return unless processor_location

      processor_location.path.partition('/triage/processor/').last
    end
  end
end
