# frozen_string_literal: true

require_relative '../processor/hellobot'
require_relative '../processor/help'
require_relative '../processor/createmarker'
require_relative '../processor/viewmarker'
require_relative '../processor/editmarker'
require_relative '../processor/viewfilehook'
require_relative '../processor/createfilehook'
require_relative '../processor/deletefilehook'
require_relative '../processor/viewsemester'

module General
  class Handler
    DEFAULT_PROCESSORS = [
      Help,
      HelloBot,
      CreateMarker,
      ViewMarker,
      EditMarker,
      ViewFileHook,
      CreateFileHook,
      DeleteFileHook,
      ViewSemester
    ].freeze
    Result = Struct.new(:message, :error)

    def initialize(event, processors: DEFAULT_PROCESSORS)
      @event = event
      @processors = processors
    end

    def process
      @processors.each do |processor|
        if "General::#{@event.description}".downcase.include?(processor.name.downcase)
          return processor.new(@event).process
        end
      end
      Help.new(@event).process
    end
  end
end
