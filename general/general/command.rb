# frozen_string_literal: true

module General
  class Command
    def initialize(description)
      @input = description.split
    end

    def command
      @input.first
    end

    def command_param(index)
      @input[index]
    end

    def command_length
      @input.length
    end
  end
end
