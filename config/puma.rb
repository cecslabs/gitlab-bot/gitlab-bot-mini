# frozen_string_literal: true

workers Integer(ENV['WEB_CONCURRENCY'] || 0)
threads_count = Integer(ENV['RAILS_MAX_THREADS'] || 1)
threads threads_count, threads_count

preload_app!

rackup      DefaultRackup if defined?(DefaultRackup)
port        ENV['PORT']     || 2000
environment ENV['RACK_ENV'] || 'production'
