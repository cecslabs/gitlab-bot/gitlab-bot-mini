FROM alpine/git as clone
WORKDIR /app
RUN git clone https://gitlab.cecs.anu.edu.au/cecslabs/gitlab-bot/gitlab-bot-mini.git


# This file is a template, and might need editing before it works on your project.
FROM ruby:2.7-alpine


# throw errors if Gemfile has been modified since Gemfile.lock
RUN bundle config --global frozen 1

WORKDIR /app/gitlab-bot-mini

COPY --from=clone /app /app/


RUN apk upgrade && apk add --no-cache git=2.36.6-r0

RUN git submodule update --init --recursive

COPY Gemfile Gemfile.lock /usr/src/app/
# Install build dependencies - required for gems with native dependencies
RUN apk add --no-cache --virtual build-deps build-base postgresql-dev python3 py3-requests && \
  bundle install


COPY . .


# change to use user rather than root
# Create a group and user
RUN addgroup -S appgroup && adduser -S appuser -G appgroup

RUN chown appuser.appgroup -R /app/gitlab-bot-mini

# Tell docker that all future commands should run as the appuser user
USER appuser

# For Rails
ENV PORT 2000
EXPOSE 2000

HEALTHCHECK --interval=5m --timeout=3s \
  CMD wget --spider --quiet --tries=1 --no-check-certificate http://127.0.0.1:2000 2>&1 | grep -q '401 Unauthorized' && exit 0 || exit 1

CMD ["puma"]
