# frozen_string_literal: true

RSpec.shared_context 'with event' do |event_class = nil|
  let(:project_id) { 123 }
  let(:iid) { 456 }
  let(:merge_request_iid) { iid }
  let(:event_actor_username) { nil }
  let(:label_names) { [] }
  let(:added_label_names) { [] }
  let(:event_attrs) { {} }
  let(:payload) { {} }
  let(:event) do
    stubbed_event(event_class: event_class, event_actor_username: event_actor_username, label_names: label_names,
                  added_label_names: added_label_names, payload: payload, **event_attrs)
  end

  def stubbed_event(event_class: nil, event_actor_username: nil, event_actor_id: nil, label_names: [],
                    added_label_names: [], payload: {}, **event_attrs)
    event_class ||= 'General::Event'
    object_kind = event_attrs[:object_kind] ||= 'issue'
    event_actor_username ||= 'root'
    event_actor_id ||= 1
    resource_author_id ||= 42

    instance_double(event_class, {
      action: 'open',
      event_actor_username: event_actor_username,
      event_actor_id: event_actor_id,
      resource_author_id: resource_author_id,
      key: "#{object_kind}.open",
      project_id: project_id,
      iid: iid,
      noteable_path: "/projects/#{project_id}/#{object_kind}s/#{iid}",
      label_names: label_names,
      added_label_names: added_label_names,
      resource_open?: true,
      url: "https://gitlab.com/group/project/-/#{object_kind}/42",
      payload: payload
    }.merge(event_attrs))
  end
end
