# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../general/processor/help'
require_relative '../../../general/general/event'

RSpec.describe General::Help do
  include_context 'with event'

  let(:comment_help) do
    <<~MARKDOWN.chomp
    ### Usage

    The following commands are available:

    #### help

    This option displays this help message.

    #### hellobot
    #{'        '}
    This option displays a greeting message from the bot.

    #### semester

    ##### viewsemester

    This option returns the semester it operates on.

    #### marker

    ##### viewmarker &lt;course&gt;

    This option returns all the attributes for the marker user.

    Example: `viewmarker comp1100`
    #{'        '}
    ##### createmarker &lt;course&gt;

    This option creates a marker for the specified comp/engn/extn course for current or upcoming semester.
    You can use `viewsemester` to check which semester is current or upcoming.
    It will not delete or remove previous marker user.

    Please check your email for a password reset email for the new user.

    Example: `createmarker comp1100`

    ##### editmarker &lt;course&gt; &lt;attribute&gt; &lt;value&gt;

    This option can change a attribute of a marker for the specified comp/engn/extn course for current or upcoming semester.
    You can use `viewsemester` to check which semester is current or upcoming.
    You can use `viewmarker` to check if an attribute is changed.

    Currently it only supports setting `projects_limit`

    Example: `editmarker comp1100 projects_limit 1000`

    #### filehook

    ##### viewfilehook &lt;course&gt;

    This option will list all the file hooks under a course.

    This command will just read from [here](https://gitlab.cecs.anu.edu.au/cecsit/gitlab-plugin/-/blob/master/adduser.yaml)

    Example: `viewfilehook comp1100`

    ##### createfilehook &#91;type&#93; &lt;course&gt; &lt;repo&gt;

    This option will a given filehook to the given repository.

    It will first check if there is a marker user available for this course in this or upcoming semester.

    You can use `viewsemester` to check which semester is current or upcoming.

    This command will just write to [here](https://gitlab.cecs.anu.edu.au/cecsit/gitlab-plugin/)

    NOTE: This command will have conflicts if more than one person is editing the repo at the same time.

    Parameters:
      type (optional): Specifies the type of file hook. If provided, it must be one of the following: [issue, user]. If not provided, a default type will be used - user.

      course: The name or identifier of the course.

      repo: The name or identifier of the repository.

    Example: 

    ```bash
    createfilehook comp1100 https://gitlab.cecs.anu.edu.au/comp1100/comp1100-lab06

    createfilehook issue comp1110 https://gitlab.cecs.anu.edu.au/comp1110/comp1110-lab06

    createfilehook user comp1100 https://gitlab.cecs.anu.edu.au/comp1110/comp1110-lab06
    ```

    ##### deletefilehook &lt;course&gt; &lt;repo&gt;

    This option will delete a marker user as a filehook to the given reository.

    It will first check if there is a marker user available for this course in this or upcoming semester.

    You can use `viewsemester` to check which semester is current or upcoming.

    Then if the marker exists, it will delete the marker to the repository.

    This command will just write to [here](https://gitlab.cecs.anu.edu.au/cecsit/gitlab-plugin/-/blob/master/adduser.yaml)

    NOTE: This command will have conflicts if more than one person is editing the repo at the same time.

    Example: `deletefilehook comp1100 https://gitlab.cecs.anu.edu.au/comp1100/comp1100-lab06`

    ---


    Please select one of the above commands to continue.

    MARKDOWN
  end

  subject { described_class.new(event) }

  it 'return help message' do
    expect_comment_request(event: event, body: comment_help) do
      subject.process
    end
  end
end
