# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../general/processor/viewfilehook'
require_relative '../../../general/general/event'

RSpec.describe General::ViewFileHook do
  include_context 'with event'

  subject { described_class.new(event) }

  context 'when the command length is 3' do
    let(:ret_comment) do
      <<~MARKDOWN.chomp
        Wrong parameter. Please type help.
      MARKDOWN
    end

    before do
      allow(event).to receive(:command).and_return(double(command_length: 3))
    end

    it 'adds a comment with the message "Wrong parameter."' do
      expect_comment_request(event: event, body: ret_comment) do
        subject.process
      end
    end
  end

  context 'when the command length is 1' do
    let(:ret_comment) do
      <<~MARKDOWN.chomp
        Wrong parameter. Please type help.
      MARKDOWN
    end

    before do
      allow(event).to receive(:command).and_return(double(command_length: 1))
    end

    it 'adds a comment with the message "Wrong parameter."' do
      expect_comment_request(event: event, body: ret_comment) do
        subject.process
      end
    end
  end

  context 'when the course is comptest' do
    let(:ret_comment) do
      <<~MARKDOWN.chomp
        course must be (comp|engn|extn)[0-9]{4}.
      MARKDOWN
    end

    before do
      allow(event).to receive(:command).and_return(double(command_length: 2, command_param: 'comptest '))
    end

    it 'adds a comment with the message "course is not (comp|engn|extn)[0-9]{4}"' do
      expect_comment_request(event: event, body: ret_comment) do
        subject.process
      end
    end
  end

  context 'when the course is aaaa1234' do
    let(:ret_comment) do
      <<~MARKDOWN.chomp
        course must be (comp|engn|extn)[0-9]{4}.
      MARKDOWN
    end

    before do
      allow(event).to receive(:command).and_return(double(command_length: 2, command_param: 'aaaa1234'))
    end

    it 'adds a comment with the message "course is not (comp|engn|extn)[0-9]{4}"' do
      expect_comment_request(event: event, body: ret_comment) do
        subject.process
      end
    end
  end

  context 'when the course is engn12345' do
    let(:ret_comment) do
      <<~MARKDOWN.chomp
        course must be (comp|engn|extn)[0-9]{4}.
      MARKDOWN
    end

    before do
      allow(event).to receive(:command).and_return(double(command_length: 2, command_param: 'engn12345'))
    end

    it 'adds a comment with the message "course is not (comp|engn|extn)[0-9]{4}"' do
      expect_comment_request(event: event, body: ret_comment) do
        subject.process
      end
    end
  end
end
