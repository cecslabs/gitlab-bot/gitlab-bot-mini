# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../general/processor/deletefilehook'
require_relative '../../../general/general/event'

RSpec.describe General::DeleteFileHook do
  include_context 'with event'

  subject { described_class.new(event) }

  context 'when the command length is 4' do
    let(:ret_comment) do
      <<~MARKDOWN.chomp
        Wrong parameter. Please type help.
      MARKDOWN
    end

    before do
      allow(event).to receive(:command).and_return(double(command_length: 4))
    end

    it 'adds a comment with the message "Wrong parameter."' do
      expect_comment_request(event: event, body: ret_comment) do
        subject.process
      end
    end
  end

  context 'when the command length is 2' do
    let(:ret_comment) do
      <<~MARKDOWN.chomp
        Wrong parameter. Please type help.
      MARKDOWN
    end

    before do
      allow(event).to receive(:command).and_return(double(command_length: 2))
    end

    it 'adds a comment with the message "Wrong parameter."' do
      expect_comment_request(event: event, body: ret_comment) do
        subject.process
      end
    end
  end

  context 'when the course doesnt match namespace' do
    let(:ret_comment) do
      <<~MARKDOWN.chomp
      Invalid command.
      MARKDOWN
    end

    before do
      allow(event).to receive(:command).and_return(double(command_length: 3, command_param: 'comp0000'))
      allow(event).to receive(:project_namespace).and_return('comp1100')
    end

    it 'adds a comment with the message "Invalid command."' do
      expect_comment_request(event: event, body: ret_comment) do
        subject.process
      end
    end
  end

  context 'when the course is comptest' do
    let(:ret_comment) do
      <<~MARKDOWN.chomp
        course must be (comp|engn|extn)[0-9]{4}.
      MARKDOWN
    end

    before do
      allow(event).to receive(:command).and_return(double(command_length: 3, command_param: 'comptest '))
      allow(event).to receive(:project_namespace).and_return('comptest')
    end

    it 'adds a comment with the message "course is not (comp|engn|extn)[0-9]{4}"' do
      expect_comment_request(event: event, body: ret_comment) do
        subject.process
      end
    end
  end

  context 'when the course is aaaa1234' do
    let(:ret_comment) do
      <<~MARKDOWN.chomp
        course must be (comp|engn|extn)[0-9]{4}.
      MARKDOWN
    end

    before do
      allow(event).to receive(:command).and_return(double(command_length: 3, command_param: 'aaaa1234'))
      allow(event).to receive(:project_namespace).and_return('aaaa1234')
    end

    it 'adds a comment with the message "course is not (comp|engn|extn)[0-9]{4}"' do
      expect_comment_request(event: event, body: ret_comment) do
        subject.process
      end
    end
  end

  context 'when the course is engn12345' do
    let(:ret_comment) do
      <<~MARKDOWN.chomp
        course must be (comp|engn|extn)[0-9]{4}.
      MARKDOWN
    end

    before do
      allow(event).to receive(:command).and_return(double(command_length: 3, command_param: 'engn12345'))
      allow(event).to receive(:project_namespace).and_return('engn12345')
    end

    it 'adds a comment with the message "course is not (comp|engn|extn)[0-9]{4}"' do
      expect_comment_request(event: event, body: ret_comment) do
        subject.process
      end
    end
  end

  context 'when the repository is not from teaching GitLab' do
    let(:ret_comment) do
      <<~MARKDOWN.chomp
        repo must be https://gitlab.cecs.anu.edu.au/<path>
      MARKDOWN
    end

    before do
      allow(event).to receive(:command).and_return(double(command_length: 3))
      allow(event.command).to receive(:command_param).with(1).and_return('engn1234')
      allow(event).to receive(:project_namespace).and_return('engn1234')
      allow(event.command).to \
        receive(:command_param).with(2).and_return('https://noexist-gitlab.cecs.anu.edu.au/not_exist')
    end

    it 'adds a comment with the message "repo must be https://gitlab.cecs.anu.edu.au/<path>"' do
      expect_comment_request(event: event, body: ret_comment) do
        subject.process
      end
    end
  end

  context 'when there is no marker user' do
    let(:ret_comment) do
      <<~MARKDOWN.chomp
        No such user. Please use `createmarker` to create a user.
      MARKDOWN
    end

    before do
      allow(event).to receive(:command).and_return(double(command_length: 3))
      allow(event.command).to receive(:command_param).with(1).and_return('engn1234')
      allow(event).to receive(:project_namespace).and_return('engn1234')
      allow(event.command).to receive(:command_param).with(2).and_return('https://gitlab.cecs.anu.edu.au/comptest/comptest-lab1')
      allow_any_instance_of(Kernel).to receive(:`).and_return('{}')
    end

    it 'adds a comment with the message "No such user. Please use `createmarker` to create a user."' do
      expect_comment_request(event: event, body: ret_comment) do
        subject.process
      end
    end
  end
end
