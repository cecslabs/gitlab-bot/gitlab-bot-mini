# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../general/processor/viewmarker'
require_relative '../../../general/general/event'

RSpec.describe General::ViewMarker do
  include_context 'with event'

  subject { described_class.new(event) }

  context 'when the command length is 3' do
    let(:ret_comment) do
      <<~MARKDOWN.chomp
        Wrong parameter. Please type help.
      MARKDOWN
    end

    before do
      allow(event).to receive(:command).and_return(double(command_length: 3))
    end

    it 'adds a comment with the message "Wrong parameter."' do
      expect_comment_request(event: event, body: ret_comment) do
        subject.process
      end
    end
  end

  context 'when the command length is 1' do
    let(:ret_comment) do
      <<~MARKDOWN.chomp
        Wrong parameter. Please type help.
      MARKDOWN
    end

    before do
      allow(event).to receive(:command).and_return(double(command_length: 1))
    end

    it 'adds a comment with the message "Wrong parameter."' do
      expect_comment_request(event: event, body: ret_comment) do
        subject.process
      end
    end
  end

  context 'when the course is comptest' do
    let(:ret_comment) do
      <<~MARKDOWN.chomp
        course must be (comp|engn|extn)[0-9]{4}.
      MARKDOWN
    end

    before do
      allow(event).to receive(:command).and_return(double(command_length: 2, command_param: 'comptest '))
    end

    it 'adds a comment with the message "course is not (comp|engn|extn)[0-9]{4}"' do
      expect_comment_request(event: event, body: ret_comment) do
        subject.process
      end
    end
  end

  context 'when the course is aaaa1234' do
    let(:ret_comment) do
      <<~MARKDOWN.chomp
        course must be (comp|engn|extn)[0-9]{4}.
      MARKDOWN
    end

    before do
      allow(event).to receive(:command).and_return(double(command_length: 2, command_param: 'aaaa1234'))
    end

    it 'adds a comment with the message "course is not (comp|engn|extn)[0-9]{4}"' do
      expect_comment_request(event: event, body: ret_comment) do
        subject.process
      end
    end
  end

  context 'when the course is engn12345' do
    let(:ret_comment) do
      <<~MARKDOWN.chomp
        course must be (comp|engn|extn)[0-9]{4}.
      MARKDOWN
    end

    before do
      allow(event).to receive(:command).and_return(double(command_length: 2, command_param: 'engn12345'))
    end

    it 'adds a comment with the message "course is not (comp|engn|extn)[0-9]{4}"' do
      expect_comment_request(event: event, body: ret_comment) do
        subject.process
      end
    end
  end

  context 'when there is no marker user' do
    let(:ret_comment) do
      <<~MARKDOWN.chomp
        No such user. Please use `createmarker` to create a user.
      MARKDOWN
    end

    before do
      allow(event).to receive(:command).and_return(double(command_length: 2))
      allow(event.command).to receive(:command_param).with(1).and_return('engn1234')
      allow_any_instance_of(Kernel).to receive(:`).and_return('{}')
    end

    it 'adds a comment with the message "No such user. Please use `createmarker` to create a user."' do
      expect_comment_request(event: event, body: ret_comment) do
        subject.process
      end
    end
  end

  context 'when it returns correct user' do
    let(:ret_comment) do
      <<~MARKDOWN.chomp
        ```yaml\n[\n  \"user\",\n  \"engn1234-marker\"\n]\n```\n
      MARKDOWN
    end

    before do
      allow(event).to receive(:command).and_return(double(command_length: 2))
      allow(event.command).to receive(:command_param).with(1).and_return('engn1234')
      allow_any_instance_of(Kernel).to receive(:`).and_return('{"user": "engn1234-marker"}')
    end

    it 'adds a comment with the message "```yaml\n[\n  \"user\",\n  \"engn1234-marker\"\n]\n```\n"' do
      expect_comment_request(event: event, body: ret_comment) do
        subject.process
      end
    end
  end
end
