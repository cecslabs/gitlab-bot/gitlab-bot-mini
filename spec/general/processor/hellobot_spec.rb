# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../general/processor/hellobot'
require_relative '../../../general/general/event'

RSpec.describe General::HelloBot do
  include_context 'with event'

  let(:comment_hellobot) do
    <<~MARKDOWN.chomp
      Hi, I am an ANU GitLab Bot. I can only respond to command you typed.
      I am not ChatGPT so I can't understand complicated requests.
      Please don't play me as I am fragile.
      If you found I didn't respond your command, please email to jie.gao@anu.edu.au.
    MARKDOWN
  end

  subject { described_class.new(event) }

  it 'return hello bot message' do
    expect_comment_request(event: event, body: comment_hellobot) do
      subject.process
    end
  end
end
