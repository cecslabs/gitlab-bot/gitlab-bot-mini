# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../general/processor/viewsemester'
require_relative '../../../general/general/event'

RSpec.describe General::ViewSemester do
  include_context 'with event'

  subject { described_class.new(event) }

  context 'when the command length is 2' do
    let(:ret_comment) do
      <<~MARKDOWN.chomp
        Wrong parameter. Please type help.
      MARKDOWN
    end

    before do
      allow(event).to receive(:command).and_return(double(command_length: 2))
    end

    it 'adds a comment with the message "Wrong parameter."' do
      expect_comment_request(event: event, body: ret_comment) do
        subject.process
      end
    end
  end

  context 'when set date to be 2022 1 1' do
    let(:ret_comment) do
      <<~MARKDOWN.chomp
        The active semester is 2022-s1.
      MARKDOWN
    end

    before do
      allow(event).to receive(:command).and_return(double(command_length: 1))
      current_time = Time.new(2022, 1, 1)
      allow(Date).to receive(:today).and_return(current_time.to_date)
      allow(Time).to receive(:now).and_return(current_time)
    end

    it 'adds a comment with the message "The active semester is 2022-s1."' do
      expect_comment_request(event: event, body: ret_comment) do
        subject.process
      end
    end
  end

  context 'when set date to be 2024 12 1' do
    let(:ret_comment) do
      <<~MARKDOWN.chomp
        The active semester is 2025-s1.
      MARKDOWN
    end

    before do
      allow(event).to receive(:command).and_return(double(command_length: 1))
      current_time = Time.new(2024, 12, 1)
      allow(Date).to receive(:today).and_return(current_time.to_date)
      allow(Time).to receive(:now).and_return(current_time)
    end

    it 'adds a comment with the message "The active semester is 2025-s1."' do
      expect_comment_request(event: event, body: ret_comment) do
        subject.process
      end
    end
  end

  context 'when set date to be 2028 7 1' do
    let(:ret_comment) do
      <<~MARKDOWN.chomp
        The active semester is 2028-s2.
      MARKDOWN
    end

    before do
      allow(event).to receive(:command).and_return(double(command_length: 1))
      current_time = Time.new(2028, 7, 1)
      allow(Date).to receive(:today).and_return(current_time.to_date)
      allow(Time).to receive(:now).and_return(current_time)
    end

    it 'adds a comment with the message "The active semester is 2028-s2."' do
      expect_comment_request(event: event, body: ret_comment) do
        subject.process
      end
    end
  end
end
