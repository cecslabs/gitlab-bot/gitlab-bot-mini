# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../general/processor/createmarker'
require_relative '../../../general/general/event'

RSpec.describe General::CreateMarker do
  include_context 'with event'

  subject { described_class.new(event) }

  context 'when the command length is 3' do
    let(:ret_comment) do
      <<~MARKDOWN.chomp
        Wrong parameter. Please type help.
      MARKDOWN
    end

    before do
      allow(event).to receive(:command).and_return(double(command_length: 3))
    end

    it 'adds a comment with the message "Wrong parameter."' do
      expect_comment_request(event: event, body: ret_comment) do
        subject.process
      end
    end
  end

  context 'when the command length is 1' do
    let(:ret_comment) do
      <<~MARKDOWN.chomp
        Wrong parameter. Please type help.
      MARKDOWN
    end

    before do
      allow(event).to receive(:command).and_return(double(command_length: 1))
    end

    it 'adds a comment with the message "Wrong parameter."' do
      expect_comment_request(event: event, body: ret_comment) do
        subject.process
      end
    end
  end

  context 'when the course doesnt match namespace' do
    let(:ret_comment) do
      <<~MARKDOWN.chomp
      Invalid command.
      MARKDOWN
    end

    before do
      allow(event).to receive(:command).and_return(double(command_length: 2, command_param: 'comp0000'))
      allow(event).to receive(:project_namespace).and_return('comp1100')
    end

    it 'adds a comment with the message "Invalid command."' do
      expect_comment_request(event: event, body: ret_comment) do
        subject.process
      end
    end
  end

  context 'when the course is comptest' do
    let(:ret_comment) do
      <<~MARKDOWN.chomp
        course must be (comp|engn|extn)[0-9]{4}.
      MARKDOWN
    end

    before do
      allow(event).to receive(:command).and_return(double(command_length: 2, command_param: 'comptest'))
      allow(event).to receive(:project_namespace).and_return('comptest')
    end

    it 'adds a comment with the message "course is not (comp|engn|extn)[0-9]{4}"' do
      expect_comment_request(event: event, body: ret_comment) do
        subject.process
      end
    end
  end

  context 'when the course is aaaa1234' do
    let(:ret_comment) do
      <<~MARKDOWN.chomp
        course must be (comp|engn|extn)[0-9]{4}.
      MARKDOWN
    end

    before do
      allow(event).to receive(:command).and_return(double(command_length: 2, command_param: 'aaaa1234'))
      allow(event).to receive(:project_namespace).and_return('aaaa1234')
    end

    it 'adds a comment with the message "course is not (comp|engn|extn)[0-9]{4}"' do
      expect_comment_request(event: event, body: ret_comment) do
        subject.process
      end
    end
  end

  context 'when the course is engn12345' do
    let(:ret_comment) do
      <<~MARKDOWN.chomp
        course must be (comp|engn|extn)[0-9]{4}.
      MARKDOWN
    end

    before do
      allow(event).to receive(:command).and_return(double(command_length: 2, command_param: 'engn12345'))
      allow(event).to receive(:project_namespace).and_return('engn12345')
    end

    it 'adds a comment with the message "course is not (comp|engn|extn)[0-9]{4}"' do
      expect_comment_request(event: event, body: ret_comment) do
        subject.process
      end
    end
  end

  context 'when python program has an error' do
    year = Date.today.year
    current_month = Date.today.month
    if [12, 1, 2, 3, 4, 5, 6].include?(current_month)
      semester = 's1'
    elsif [7, 8, 9, 10, 11].include?(current_month)
      semester = 's2'
    end
    year = Time.now.year
    year += 1 if current_month == 12
    year = year.to_s

    let(:ret_comment) do
      <<~MARKDOWN.chomp
        There is an error when creating engn1234-#{year}-#{semester}-marker. The message is: Some error occurred
      MARKDOWN
    end

    before do
      allow(event).to receive(:command).and_return(double(command_length: 2, command_param: 'engn1234'))
      allow(event).to receive(:project_namespace).and_return('engn1234')
      allow_any_instance_of(Kernel).to receive(:`).and_return('{"message":"Some error occurred"}')
    end

    it 'adds a comment with the message "There is an error when creating"' do
      expect_comment_request(event: event, body: ret_comment) do
        subject.process
      end
    end
  end

  context 'when it returns correct result' do
    year = Date.today.year
    current_month = Date.today.month
    if [12, 1, 2, 3, 4, 5, 6].include?(current_month)
      semester = 's1'
    elsif [7, 8, 9, 10, 11].include?(current_month)
      semester = 's2'
    end
    year = Time.now.year
    year += 1 if current_month == 12
    year = year.to_s

    let(:ret_comment) do
      <<~MARKDOWN.chomp
        engn1234-#{year}-#{semester}-marker is created! There should be an email sent to your inbox for resetting password. If you cannot find it, please check the quarantine.
      MARKDOWN
    end

    before do
      allow(event).to receive(:command).and_return(double(command_length: 2, command_param: 'engn1234'))
      allow(event).to receive(:project_namespace).and_return('engn1234')
      allow_any_instance_of(Kernel).to receive(:`).and_return('{"result":"Success!!"}')
    end

    it 'adds a comment with the message "There is an error when creating"' do
      expect_comment_request(event: event, body: ret_comment) do
        subject.process
      end
    end
  end
end
