# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../general/processor/editmarker'
require_relative '../../../general/general/event'

RSpec.describe General::EditMarker do
  include_context 'with event'

  subject { described_class.new(event) }

  context 'when the command length is 5' do
    let(:ret_comment) do
      <<~MARKDOWN.chomp
        Wrong parameter. Please type help.
      MARKDOWN
    end

    before do
      allow(event).to receive(:command).and_return(double(command_length: 5))
    end

    it 'adds a comment with the message "Wrong parameter."' do
      expect_comment_request(event: event, body: ret_comment) do
        subject.process
      end
    end
  end

  context 'when the command length is 3' do
    let(:ret_comment) do
      <<~MARKDOWN.chomp
        Wrong parameter. Please type help.
      MARKDOWN
    end

    before do
      allow(event).to receive(:command).and_return(double(command_length: 3))
    end

    it 'adds a comment with the message "Wrong parameter."' do
      expect_comment_request(event: event, body: ret_comment) do
        subject.process
      end
    end
  end

  context 'when the course doesnt match namespace' do
    let(:ret_comment) do
      <<~MARKDOWN.chomp
      Invalid command.
      MARKDOWN
    end

    before do
      allow(event).to receive(:command).and_return(double(command_length: 4, command_param: 'comp0000'))
      allow(event).to receive(:project_namespace).and_return('comp1100')
    end

    it 'adds a comment with the message "Invalid command."' do
      expect_comment_request(event: event, body: ret_comment) do
        subject.process
      end
    end
  end

  context 'when the course is comptest' do
    let(:ret_comment) do
      <<~MARKDOWN.chomp
        course must be (comp|engn|extn)[0-9]{4}.
      MARKDOWN
    end

    before do
      allow(event).to receive(:command).and_return(double(command_length: 4, command_param: 'comptest'))
      allow(event).to receive(:project_namespace).and_return('comptest')
    end

    it 'adds a comment with the message "course is not (comp|engn|extn)[0-9]{4}"' do
      expect_comment_request(event: event, body: ret_comment) do
        subject.process
      end
    end
  end

  context 'when the course is aaaa1234' do
    let(:ret_comment) do
      <<~MARKDOWN.chomp
        course must be (comp|engn|extn)[0-9]{4}.
      MARKDOWN
    end

    before do
      allow(event).to receive(:command).and_return(double(command_length: 4, command_param: 'aaaa1234'))
      allow(event).to receive(:project_namespace).and_return('aaaa1234')
    end

    it 'adds a comment with the message "course is not (comp|engn|extn)[0-9]{4}"' do
      expect_comment_request(event: event, body: ret_comment) do
        subject.process
      end
    end
  end

  context 'when the course is engn12345' do
    let(:ret_comment) do
      <<~MARKDOWN.chomp
        course must be (comp|engn|extn)[0-9]{4}.
      MARKDOWN
    end

    before do
      allow(event).to receive(:command).and_return(double(command_length: 4, command_param: 'engn12345'))
      allow(event).to receive(:project_namespace).and_return('engn12345')
    end

    it 'adds a comment with the message "course is not (comp|engn|extn)[0-9]{4}"' do
      expect_comment_request(event: event, body: ret_comment) do
        subject.process
      end
    end
  end

  context 'when the key is not set correctly' do
    let(:ret_comment) do
      <<~MARKDOWN.chomp
        We can only set up `projects_limit` now for the marker user.
      MARKDOWN
    end

    before do
      allow(event).to receive(:command).and_return(double(command_length: 4))
      allow(event.command).to receive(:command_param).with(1).and_return('engn1234')
      allow(event).to receive(:project_namespace).and_return('engn1234')
      allow(event.command).to receive(:command_param).with(2).and_return('projects_limit1')
      allow(event.command).to receive(:command_param).with(3).and_return('100')
      allow_any_instance_of(Kernel).to receive(:`).and_return('{}')
    end

    it 'adds a comment with the message "We can only set up `projects_limit` now for the marker user."' do
      expect_comment_request(event: event, body: ret_comment) do
        subject.process
      end
    end
  end

  context 'when the project limit is equal to 0' do
    let(:ret_comment) do
      <<~MARKDOWN.chomp
        The project limit must be greater than 0 and smaller than 10000.
      MARKDOWN
    end

    before do
      allow(event).to receive(:command).and_return(double(command_length: 4))
      allow(event.command).to receive(:command_param).with(1).and_return('engn1234')
      allow(event).to receive(:project_namespace).and_return('engn1234')
      allow(event.command).to receive(:command_param).with(2).and_return('projects_limit')
      allow(event.command).to receive(:command_param).with(3).and_return('0')
      allow_any_instance_of(Kernel).to receive(:`).and_return('{}')
    end

    it 'adds a comment with the message "The project limit must be greater than 0 and smaller than 10000."' do
      expect_comment_request(event: event, body: ret_comment) do
        subject.process
      end
    end
  end

  context 'when the project limit is equal to 10000' do
    let(:ret_comment) do
      <<~MARKDOWN.chomp
        The project limit must be greater than 0 and smaller than 10000.
      MARKDOWN
    end

    before do
      allow(event).to receive(:command).and_return(double(command_length: 4))
      allow(event.command).to receive(:command_param).with(1).and_return('engn1234')
      allow(event).to receive(:project_namespace).and_return('engn1234')
      allow(event.command).to receive(:command_param).with(2).and_return('projects_limit')
      allow(event.command).to receive(:command_param).with(3).and_return('10000')
      allow_any_instance_of(Kernel).to receive(:`).and_return('{}')
    end

    it 'adds a comment with the message "The project limit must be greater than 0 and smaller than 10000."' do
      expect_comment_request(event: event, body: ret_comment) do
        subject.process
      end
    end
  end

  context 'when there is no such marker user' do
    year = Date.today.year
    current_month = Date.today.month
    if [12, 1, 2, 3, 4, 5, 6].include?(current_month)
      semester = 's1'
    elsif [7, 8, 9, 10, 11].include?(current_month)
      semester = 's2'
    end
    year = Time.now.year
    year += 1 if current_month == 12
    year = year.to_s

    let(:ret_comment) do
      <<~MARKDOWN.chomp
        There is an error when creating engn1234-#{year}-#{semester}-marker. The message is: no such user
      MARKDOWN
    end

    before do
      allow(event).to receive(:command).and_return(double(command_length: 4))
      allow(event.command).to receive(:command_param).with(1).and_return('engn1234')
      allow(event).to receive(:project_namespace).and_return('engn1234')
      allow(event.command).to receive(:command_param).with(2).and_return('projects_limit')
      allow(event.command).to receive(:command_param).with(3).and_return('1000')
      allow_any_instance_of(Kernel).to receive(:`).and_return('{"error": "no such user"}')
    end

    it %(adds a comment with the message "There is an error when creating engn1234-#{year}-#{semester}-marker. The message is: no such user") do
      expect_comment_request(event: event, body: ret_comment) do
        subject.process
      end
    end
  end

  context 'when everything is correct' do
    year = Date.today.year
    current_month = Date.today.month
    if [12, 1, 2, 3, 4, 5, 6].include?(current_month)
      semester = 's1'
    elsif [7, 8, 9, 10, 11].include?(current_month)
      semester = 's2'
    end
    year = Time.now.year
    year += 1 if current_month == 12
    year = year.to_s

    let(:ret_comment) do
      <<~MARKDOWN.chomp
        The value of projects_limit is changed to 1000 for engn1234-#{year}-#{semester}-marker. Use viewmarker to check the change.
      MARKDOWN
    end

    before do
      allow(event).to receive(:command).and_return(double(command_length: 4))
      allow(event.command).to receive(:command_param).with(1).and_return('engn1234')
      allow(event).to receive(:project_namespace).and_return('engn1234')
      allow(event.command).to receive(:command_param).with(2).and_return('projects_limit')
      allow(event.command).to receive(:command_param).with(3).and_return('1000')
      allow_any_instance_of(Kernel).to receive(:`).and_return('{"message": "success"}')
    end

    it %(adds a comment with the message "The value of projects_limit is changed to 1000 for engn1234-#{year}-#{semester}-marker. Use viewmarker to check the change.") do
      expect_comment_request(event: event, body: ret_comment) do
        subject.process
      end
    end
  end
end
